var fs = require('fs')
    , http = require('http')
    , socketio = require('socket.io');

global.jogadores = [];
global.desafio = { "pergunta":"", "resposta":0 };

function gerar_desafio() {
	var op1 = (100*Math.random()).toFixed();
	var op2 = (100*Math.random()).toFixed();
	global.desafio.pergunta=op1+"+"+op2+"?";
	global.desafio.resposta = op1+op2;
}
 
var server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-type': 'text/html'});
    res.end(fs.readFileSync(__dirname + '/index.html'));
}).listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});
 
socketio.listen(server).on('connection', function (socket) {
    socket.on('message', function (msg) {
        console.log('Message Received: ', msg);
        socket.broadcast.emit('message', msg);
    });
    socket.on('logar', function (msg) {
        console.log('Usuario logado: '+ msg);
		global.jogadores.push({'id': socket.id, 
							   'nome': msg, 
							   'pontos': 0});
        socket.broadcast.emit('message', 'Jogador '+msg+' entrou no jogo');
    });
    socket.on('listar', function (msg) {
		var lista = '';
        console.log('Usuario: '+ msg+'listou jogadores');
		global.jogadores.forEach( function( valor ) {
			lista += valor.nome+": "+valor.pontos+"; ";
		});
        socket.emit('message', lista);			
    });
    socket.on('iniciar', function (nome) {
        console.log('Usuario: '+ nome+' iniciou o jogo');
		gerar_desafio();
        socket.emit('message', 'o jogo iniciou. '+global.desafio.pergunta);
        socket.broadcast.emit('message', 'o jogo iniciou'+global.desafio.pergunta);			
    });
    socket.on('responder', function ( objResposta ) {
        console.log('Usuario: '+ objResposta.nome+' respondeu o desafio');
		if ( objResposta.resposta == 
				global.desafio.resposta ) {
			global.jogadores.forEach( function( jogador ) {
				if ( jogador.socketid == socket.id ) 
					jogador.pontos++;
				else
					jogador.pontos--;
			});
		}
    });

});
